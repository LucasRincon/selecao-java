# Seleção-Java-API
> API criada para teste de seleção.

A API desenvolvida permite a utilização de CRUD para usuários e CRUD para classe de histórico de preços de combustível além de outros recursos customizados para a mesma. A classe de histórico foi criada baseada nos dados do arquivo csv que pode ser baixado através do link -> http://www.anp.gov.br/images/dadosabertos/precos/2018-1_CA.csv

A API foi desenvoldia utilizando as tecnologias:
H2 Banco de dados.
Linguagem JAVA com framework Spring.

A aplicação conta também com uma doecumentação interativa através do swagger.

## Instalação e execução

Crie um clone ou baixe um .zip da aplicação.

Importe a aplicação com sua IDE de preferencia. Após a importação execute a seguinte classe:
```sh
SelecaoJavaApiApplication.class
```

Caso prefira execute por linha de comando, abrindo o terminal e execute o comando:
```sh
mvwn spring-boot:run
```
obs.: Certifique-se de estar com a variavel de ambiente JAVA_HOME devidamente configurada. 

## Banco de dados

A aplicação conta com o uso do bando de dados H2 direto em memória, o qual é executado automaticamente junto com a aplicação.

Para acessar seu console, vá até o seguinte caminho e clique em run:
```sh
http://localhost:8080/api/v1/h2
```
obs: Lembre-se, como o banco está sendo executado em memória, sempre que a aplicação parar, os dados serão perdidos.

## Requisições na API

Para realização das requisições, foi adicionado ao projeto um arquivo Requests-API.rest com as requisições no formato http.

Para facilitar as chamadas é recomendado o uso da extensão Rest Client no Visual Studio Code.

Caso prefira, é possivel realizá-las diretamente através do swagger pelo caminho (http://localhost:8080/api/v1/swagger-ui.html)

## Contato

Lucas Rincon
lucas_rincon26@hotmail.com
[Gitlab](https://gitlab.com/LucasRincon)