package com.indrajpa.selecaojavaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SelecaoJavaApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SelecaoJavaApiApplication.class, args);
	}

}
