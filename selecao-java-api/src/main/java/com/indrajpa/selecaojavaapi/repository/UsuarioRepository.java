package com.indrajpa.selecaojavaapi.repository;


import com.indrajpa.selecaojavaapi.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
