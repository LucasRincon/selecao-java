package com.indrajpa.selecaojavaapi.controller;


import com.indrajpa.selecaojavaapi.dto.UsuarioDTO;
import com.indrajpa.selecaojavaapi.mapper.UsuarioMapper;
import com.indrajpa.selecaojavaapi.service.UsuarioService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @ApiOperation(value = "Cadastra um novo usuário")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Novo usuário inserido"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @RequestMapping(method = RequestMethod.POST, consumes = {"application/json"})
    ResponseEntity<?> inserirUsuario(@RequestBody @Valid UsuarioDTO usuarioDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(usuarioService.insereUsuario(UsuarioMapper.fromDTO(usuarioDTO)));
    }

    @ApiOperation(value = "Retorna uma lista de usuários")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retornada lista de usuários"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"})
    ResponseEntity<?> listarUsuarios() {
        return ResponseEntity.ok(usuarioService.listaUsuarios());
    }

    @ApiOperation(value = "Retorna os dados de um usuários")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retornado dados de um usuário"),
            @ApiResponse(code = 400, message = "O usuário não existe"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @RequestMapping(value="/{id}", method = RequestMethod.GET, produces = {"application/json"})
    ResponseEntity<?> buscarUsuario(@PathVariable("id") Long id) {
        return ResponseEntity.ok(usuarioService.buscaUsuario(id));
    }

    @ApiOperation(value = "Altera os dados de um usuários")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Alterado os dados de um usuário"),
            @ApiResponse(code = 400, message = "O usuário não existe"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @RequestMapping(value="/{id}", method = RequestMethod.PUT, consumes = {"application/json"}, produces = {"application/json"})
    ResponseEntity<?> atualizarUsuario(@PathVariable("id") Long id, @RequestBody @Valid UsuarioDTO usuarioDTO) {
        return ResponseEntity.ok(usuarioService.atualizaUsuario(UsuarioMapper.fromDTO(usuarioDTO), id));
    }

    @ApiOperation(value = "Deleta os dados de um usuários")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Deletados os dados de um usuário"),
            @ApiResponse(code = 400, message = "O usuário não existe"),
            @ApiResponse(code = 500, message = "Erro interno do servidor"),
    })
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE, produces = {"application/json"})
    ResponseEntity<?> deletarUsuario(@PathVariable("id") Long id) {
        usuarioService.deletaUsuario(id);
        return ResponseEntity.ok().build();
    }
}
