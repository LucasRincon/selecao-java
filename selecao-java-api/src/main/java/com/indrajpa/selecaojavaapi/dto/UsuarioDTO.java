package com.indrajpa.selecaojavaapi.dto;

import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UsuarioDTO {

    @NotBlank(message = "{notblank.usuariodto.nome}")
    private String nome;

    @NotBlank(message = "{notblank.usuariodto.senha}")
    private String senha;
}
