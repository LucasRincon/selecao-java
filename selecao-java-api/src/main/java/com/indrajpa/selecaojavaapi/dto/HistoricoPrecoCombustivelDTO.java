package com.indrajpa.selecaojavaapi.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Data
@NoArgsConstructor
public class HistoricoPrecoCombustivelDTO {

    private String regiaoSigla;

    private String estadoSigla;

    private String municipio;

    private String revenda;

    private Long instalacaoCodigo;

    private String produto;

    private LocalDate dataDaColeta;

    private double valorDeCompra;

    private double valorDeVenda;

    private String unidadeDeMedida;

    private String bandeira;
}
