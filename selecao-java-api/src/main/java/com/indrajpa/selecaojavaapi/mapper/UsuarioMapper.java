package com.indrajpa.selecaojavaapi.mapper;

import com.indrajpa.selecaojavaapi.dto.UsuarioDTO;
import com.indrajpa.selecaojavaapi.model.Usuario;

public class UsuarioMapper {

    public static Usuario fromDTO(UsuarioDTO dto) {
        Usuario usuario = new Usuario();
        usuario.setNome(dto.getNome());
        usuario.setSenha(dto.getSenha());

        return usuario;
    }

}