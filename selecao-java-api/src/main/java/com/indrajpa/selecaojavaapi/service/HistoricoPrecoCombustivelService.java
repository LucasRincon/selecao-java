package com.indrajpa.selecaojavaapi.service;

import com.indrajpa.selecaojavaapi.model.HistoricoPrecoCombustivel;
import com.indrajpa.selecaojavaapi.repository.HistoricoPrecoCombustivelRepository;
import com.indrajpa.selecaojavaapi.mapper.HistoricoPrecoCombustivelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

import static com.indrajpa.selecaojavaapi.exception.ExceptionDefault.checkThrow;
import static com.indrajpa.selecaojavaapi.exception.ExceptionsMessagesEnum.*;

@Service
public class HistoricoPrecoCombustivelService {

    @Autowired
    HistoricoPrecoCombustivelRepository historicoPrecoCombustivelRepository;

    public HistoricoPrecoCombustivel insereDado(HistoricoPrecoCombustivel historicoPrecoCombustivel){
        return historicoPrecoCombustivelRepository.save(historicoPrecoCombustivel);
    }

    public Page<HistoricoPrecoCombustivel> listaDados(int page, int size){
        return historicoPrecoCombustivelRepository.findAll(PageRequest.of(page, size));
    }

    public HistoricoPrecoCombustivel buscaDado(Long id){
        Optional<HistoricoPrecoCombustivel> optionalHistoricoPrecoCombustivel = historicoPrecoCombustivelRepository.findById(id);
        checkThrow(!optionalHistoricoPrecoCombustivel.isPresent(), INFO_NAO_ENCONTRADA);

        HistoricoPrecoCombustivel historicoPrecoCombustivel = optionalHistoricoPrecoCombustivel.get();

        return historicoPrecoCombustivelRepository.save(historicoPrecoCombustivel);
    }

    public HistoricoPrecoCombustivel atualizaDado(HistoricoPrecoCombustivel novoDado, Long id){
        Optional<HistoricoPrecoCombustivel> optionalHistoricoPrecoCombustivel = historicoPrecoCombustivelRepository.findById(id);
        checkThrow(!optionalHistoricoPrecoCombustivel.isPresent(), INFO_NAO_ENCONTRADA);

        HistoricoPrecoCombustivel historicoPrecoCombustivel = optionalHistoricoPrecoCombustivel.get();

        historicoPrecoCombustivel.setRegiaoSigla(novoDado.getRegiaoSigla());
        historicoPrecoCombustivel.setEstadoSigla(novoDado.getEstadoSigla());
        historicoPrecoCombustivel.setMunicipio(novoDado.getMunicipio());
        historicoPrecoCombustivel.setRevenda(novoDado.getRevenda());
        historicoPrecoCombustivel.setInstalacaoCodigo(novoDado.getInstalacaoCodigo());
        historicoPrecoCombustivel.setProduto(novoDado.getProduto());
        historicoPrecoCombustivel.setDataDaColeta(novoDado.getDataDaColeta());
        historicoPrecoCombustivel.setValorDeCompra(novoDado.getValorDeCompra());
        historicoPrecoCombustivel.setValorDeVenda(novoDado.getValorDeVenda());
        historicoPrecoCombustivel.setUnidadeDeMedida(novoDado.getUnidadeDeMedida());
        historicoPrecoCombustivel.setBandeira(novoDado.getBandeira());

        return historicoPrecoCombustivelRepository.save(historicoPrecoCombustivel);
    }

    public void deletaDado(Long id) {
        Optional<HistoricoPrecoCombustivel> optionalHistoricoPrecoCombustivel = historicoPrecoCombustivelRepository.findById(id);
        checkThrow(!optionalHistoricoPrecoCombustivel.isPresent(), INFO_NAO_ENCONTRADA);
        historicoPrecoCombustivelRepository.deleteById(id);
    }

    public String importaCSV(MultipartFile historicoCSV) throws IOException {
        List<HistoricoPrecoCombustivel> historico = csvParaHistorico(historicoCSV);
        historicoPrecoCombustivelRepository.saveAll(historico);

        return "Dados de importação inseridos com sucesso";
    }

    private List<HistoricoPrecoCombustivel> csvParaHistorico(MultipartFile csv) throws IOException {
        BufferedReader br = null;
        try{
            InputStream is = csv.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
        }
        catch (IOException e){
            ERRO_LEITURA_ARQUIVO.raise();
        }
        checkThrow(br == null, ERRO_LEITURA_ARQUIVO);

        List<HistoricoPrecoCombustivel> historico = new ArrayList<>();
        String linha = br.readLine();
        while ((linha = br.readLine()) != null) {
            if(linha.contains("  ")){
                linha = linha.replaceAll(",", ".");
                linha = linha.replaceAll("  ", ",");
                String[] linhasplit = linha.split(",");
                if(linhasplit.length==11){
                    historico.add(HistoricoPrecoCombustivelMapper.stringToHistorico(linha));
                }
            }
            else {
                String[] linhasplit = linha.split(",");
                if (linhasplit.length == 11) {
                    historico.add(HistoricoPrecoCombustivelMapper.stringToHistorico(linha));
                }
            }
        }
        return historico;
    }

    public Optional<Double> buscaMediaPrecoPorMunicipio(String municipio) {
        Optional<Double> media = historicoPrecoCombustivelRepository.findAvgValorDeVendaByMunicipio(municipio);
        checkThrow(!media.isPresent(), ERRO_BUSCAR_AVGVALORVENDA);
        return media;
    }

    public Page<HistoricoPrecoCombustivel> listaPorRegiaoSigla(String regiaoSigla, int page, int size) {
        return historicoPrecoCombustivelRepository.findAllByRegiaoSiglaContainingIgnoreCase(regiaoSigla, PageRequest.of(page, size));
    }

    public Map<String, Page<HistoricoPrecoCombustivel>> agrupaPorBandeira(int page, int size) {
        List<String> bandeiras = historicoPrecoCombustivelRepository.findDistinctBandeira();

        Map<String, Page<HistoricoPrecoCombustivel>> mapBandeiras = new HashMap<>();

        for (String bandeira: bandeiras) {
            Page<HistoricoPrecoCombustivel> historicoBandeira = historicoPrecoCombustivelRepository.findAllByBandeira(bandeira, PageRequest.of(page, size));
            mapBandeiras.put(bandeira, historicoBandeira);
        }
        return mapBandeiras;
    }

    public Map<LocalDate, Page<HistoricoPrecoCombustivel>> agrupaPorDataDaColeta(int page, int size) {
        List<Date> datas = historicoPrecoCombustivelRepository.findDistinctDataDaColeta();

        Map<LocalDate, Page<HistoricoPrecoCombustivel>> mapDatas = new HashMap<>();

        for (Date data: datas) {
            Page<HistoricoPrecoCombustivel> historicoBandeira = historicoPrecoCombustivelRepository.findAllByDataDaColeta(data.toLocalDate(), PageRequest.of(page, size));
            mapDatas.put(data.toLocalDate(), historicoBandeira);
        }
        return mapDatas;
    }

    public Map<String, Optional<Double>> buscaMediaCompraVendaByMunicipio(String municipio) {
        Map<String, Optional<Double>> mapResponse = new HashMap<>();

        Optional<Double> mediaValorCompra = historicoPrecoCombustivelRepository.findAvgValorDeCompraByMunicipio(municipio);
        checkThrow(!mediaValorCompra.isPresent(), ERRO_BUSCAR_AVGVALORCOMPRA);

        Optional<Double> mediaValorVenda = historicoPrecoCombustivelRepository.findAvgValorDeVendaByMunicipio(municipio);
        checkThrow(!mediaValorVenda.isPresent(), ERRO_BUSCAR_AVGVALORVENDA);

        mapResponse.put("valorDeCompra", mediaValorCompra);
        mapResponse.put("valorDeVenda", mediaValorVenda);

        return mapResponse;
    }

    public Map<String, Optional<Double>> buscaMediaCompraVendaBybandeira(String bandeira) {
        Map<String, Optional<Double>> mapResponse = new HashMap<>();

        Optional<Double> mediaValorCompra = historicoPrecoCombustivelRepository.findAvgValorDeCompraByBandeira(bandeira);
        checkThrow(!mediaValorCompra.isPresent(), ERRO_BUSCAR_AVGVALORCOMPRA);

        Optional<Double> mediaValorVenda = historicoPrecoCombustivelRepository.findAvgValorDeVendaByBandeira(bandeira);
        checkThrow(!mediaValorVenda.isPresent(), ERRO_BUSCAR_AVGVALORVENDA);

        mapResponse.put("valorDeCompra", mediaValorCompra);
        mapResponse.put("valorDeVenda", mediaValorVenda);

        return mapResponse;
    }
}
