package com.indrajpa.selecaojavaapi.service;

import static com.indrajpa.selecaojavaapi.exception.ExceptionDefault.checkThrow;
import static com.indrajpa.selecaojavaapi.exception.ExceptionsMessagesEnum.USUARIO_NAO_ENCONTRADO;

import com.indrajpa.selecaojavaapi.model.Usuario;
import com.indrajpa.selecaojavaapi.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario insereUsuario(Usuario usuario){
        return usuarioRepository.save(usuario);
    }

    public List<Usuario> listaUsuarios(){
        return usuarioRepository.findAll();
    }

    public Usuario buscaUsuario(Long id){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        checkThrow(!optionalUsuario.isPresent(), USUARIO_NAO_ENCONTRADO);

        Usuario usuario = optionalUsuario.get();

        return usuarioRepository.save(usuario);
    }

    public Usuario atualizaUsuario(Usuario novoUsuario, Long id){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        checkThrow(!optionalUsuario.isPresent(), USUARIO_NAO_ENCONTRADO);

        Usuario usuario = optionalUsuario.get();

        usuario.setNome(novoUsuario.getNome());
        usuario.setSenha(novoUsuario.getSenha());

        return usuarioRepository.save(usuario);
    }

    public void deletaUsuario(Long id) {
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
        checkThrow(!optionalUsuario.isPresent(), USUARIO_NAO_ENCONTRADO);
        usuarioRepository.deleteById(id);
    }
}
